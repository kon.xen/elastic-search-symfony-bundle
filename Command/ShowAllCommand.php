<?php

namespace SearchBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class ShowAllCommand extends Command {

	protected static $defaultName = 'search:show-all';

	private $server;
    private $port;  

     function __construct( $elastic_server, $elastic_port )
    {            
        $this->server = $elastic_server;
        $this->port = $elastic_port;

        parent::__construct();	
    }  

	protected function configure()
	{
		$this
			->setDescription('displays information of elastic search indicies')
			->setHelp('This command displays information of elastic search indicies');
	}

	protected function execute(InputInterface $input, OutputInterface $output) 
	{
		$request = curl_init();

		$url     = $this->server. ':' . $this->port  . '/' . '_cat/indices?v&pretty';
		$options = [
			CURLOPT_URL            => $url,
			CURLOPT_RETURNTRANSFER => 1
		];

		curl_setopt_array($request, $options);

		$result = curl_exec($request);

		if (curl_errno($request)) {
			echo 'Error:'.curl_error($request);
		}

		curl_close($request);

		$output->writeln('<fg=green>' . $result . '</>');
	}
}
