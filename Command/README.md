# With this commands you can :
### modify json files to be used with the elastic search API and bulk index them
### bulk index .json files in to elastic search.
### delete an index allready on ellastic search.
### display indicies loaded on elastic search.

***Requirements:***
* In order to use PHP's cURL functions you need to install the » libcurl package. PHP requires libcurl version 7.10.5 or later.
* CURL enabled in PHP
* a .json file with no more than 5 fields ! 
* **ALL JSON FILES MUST BE IN** ***SearchBundle/Jsons/*** **the path is fixed** !
	

**usage:**

####To modify a json file and perform bulk index.
run:
```bin/console ***search:batch-index*** *index* *file.json*```

**i.e** ```/app $ bin/console search:batch-index articles waCampaigns.json```

What does it do:

it will take the json, and modify it by adding an extra line before each index so *elastic search* can understand it...
Then will save it with as a new file with suffix *_new*.
If the original file is < 75mb the file name will be *waCampaigns_new.json*
Depending on the size of the file ( filesize >= 75mb ) will breake it in to x number of chunks and save each with the suffix incremented. 
example:  *waCampaigns_new1*  *waCampaigns_new2* and so forth.
Then will take each file and "load" it to elastic search. Simples :)

After indexing, the new files will remain in the *Json* folder.

####To only bulk index a .json file that has been modified.
(you allready indexed it before and have the modified file, *waCampaigns_new.json*)
run:
bin/console ***search:load-index*** *index* *file.json* 
**i.e**  ```/app $ bin/console search:load-index campaigns waCampaigns_new.json ```

if you have more than one files:
() because the file was freaking huge... )
```bin/console ***search:load-index*** *index* *file1.json* *file2.json* *file3.json*... ```

**i.e**  ```/app $ bin/console search:load-index campaigns waCampaigns_new1.json waCampaigns_new2.json waCampaigns_new3.json  ```


####To delete an index.
```bin/console ***search:delete-index*** *index*```

**i.e**  ```/app $ bin/console search:delete-index campaigns```

####To check is all done / ok.
```bin/console search:show-all```



**NOTE** 

**The command makes Symfony to run out of memmory on files larger than 6.5Mb....***
don't know how to make workers etc... so
you may be better off using the stand allone version.
from here:  https://gitlab.com/kon.xen/elascripts  
No symfony required. simple php commands

Have fun :)