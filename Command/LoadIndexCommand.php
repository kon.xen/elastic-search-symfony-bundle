<?php

namespace SearchBundle\Command;

use SearchBundle\Command\CreateIndex as Create;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadIndexCommand extends Command 
{
	protected static $defaultName = 'search:load-index';
	
	public $files;
	public $index;
	private $defaultpath;
    private $server;
    private $port;  

    function __construct( $jsonpath ,  $elastic_server , $elastic_port )
    {            
        $this->server = $elastic_server;
        $this->port = $elastic_port;
        $this->defaultpath = dir($jsonpath);
       	
       	parent::__construct();
    }  


	protected function configure() {
		$this
			->setDescription('Creates and batch populates a new elastic-search index from a json file')
			->setHelp('This command allows you to create and populate an elastic search index from existing modified json(s)')
			->addArgument('index', InputArgument::REQUIRED, 'the elastic-search index to create and populate')
			->addArgument('files', InputArgument::REQUIRED, 'the modified json files to populate from (separate multiple names with a space)?');
	}

	protected function initialize(InputInterface $input, OutputInterface $output)
    {			
		$path  = realpath($this->defaultpath->path);

		$this->files = self::seperateNames(
								$input->getArgument('files'),
								$path
							);
		
		$this->index = trim( $input->getArgument('index') );
    }

	protected function interact(InputInterface $input, OutputInterface $output)
    {        
        foreach ($this->files as $value) {

        	 if ( !is_file( $value ) ) {
                $output->writeln('<bg=yellow;options=bold>! error :  File '. $value . ' not found </>');              
                die;
            }        	
        }       
    }

	protected function execute(InputInterface $input, OutputInterface $output) {
		$create = new Create;

		//print_r($this->files);//testing

		$create->createIndex($this->files, $this->index, $this->server, $this->port );
	}

	function seperateNames($files, $path) {
		$files_temp = explode(" ", $files);
		$keys       = count($files_temp);
		$names      = [];

		for ($i = 1; $i <= $keys; $i++) {
			foreach ($files_temp as $key => $value) {
				$names[$key+1] = $path.'/'.$value;
			}
		}

		return $names;
	}
}