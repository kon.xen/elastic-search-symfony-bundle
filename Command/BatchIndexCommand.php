<?php

namespace SearchBundle\Command;

use SearchBundle\Command\CreateIndex as Create;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BatchIndexCommand extends Command 
{
	
	protected static $defaultName = 'search:batch-index';

	private $files;
    private $index;
    private $defaultpath;
    private $file_names_to_index = [];

    private $server;
    private $port;  

    function __construct( $jsonpath ,  $elastic_server, $elastic_port )
    {            
        $this->server = $elastic_server;
        $this->port = $elastic_port;
        $this->defaultpath = dir($jsonpath);
       	
       	parent::__construct();
    }  

	protected function configure( )
    {                       
		$this
			->setDescription( 'Creates and batch populates a new elastic-search index from a json file' )
			->setHelp( 'This command allows you to create and populate an elastic search index' )
            ->addArgument( 'index', InputArgument::REQUIRED, 'the elastic-search index to create and populate' )
			->addArgument( 'file',  InputArgument::REQUIRED, 'the json file to populate from' );
			
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {           
        // $this->defaultpath = dir('src/SearchBundle/Jsons/'); // needs yml ?
        $path  = realpath($this->defaultpath->path);

        $this->file = $path . '/' . trim( $input->getArgument( 'file' ) ); 
        
        $this->index = trim( $input->getArgument( 'index' ) );
    }


    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if ( !$this->file ) {

                $output->writeln('<bg=yellow;options=bold>! error :  File not found </>');              
                die;
            }
    }

	protected function execute(InputInterface $input, OutputInterface $output)
    {
		$create = new Create;

		$output->writeln( '<bg=red;options=bold>=== any existing files will be overwriten </>, procced ? y / n : ');
		self::ask_confirmation();

		$that_many_files = self::evaluateSize($this->file);

		self::modifyFile($this->file, $that_many_files);
        
	    $create->createIndex( $this->file_names_to_index, $this->index, $this->server, $this->port );
	}

	
    function ask_confirmation() {
		$answer = trim(strtolower(fgets(STDIN)));
		$this->check_confirmation($answer);
	}

	
    function check_confirmation($answer) {
		switch ($answer) {
			case 'y':
				return;
				break;
			case 'n':
				echo '=== terminating...';
				die;
				break;

			default:
				echo '=== please enter y or n ';
				$this->ask_confirmation();
				break;
		}
	}

	
    /**
	 * summary: modifies the json file so can be imported in elasticSearch
	 * @var $file
	 * @var $that_many_files
	 **/
	function modifyFile($file, $that_many_files) {
		$starting_time = gettimeofday();

		$file_contents = file($file, FILE_IGNORE_NEW_LINES);
		$lines         = count($file_contents);

		if ($that_many_files >= 2) {

			$file_chunks  = calculateChunks($file, $lines);
			$no_of_chunks = count($file_chunks);

			for ($i = 1; $i <= $no_of_chunks; $i++) {

				$new_filename = self::newFilename($file, $i);
				//stristr( $file, '.', true ) . "_new". $i . ".json";
				$this->file_names_to_index[$i] = $new_filename;
				$line_counter                  = 0;

				self::checkFileExists($new_filename);

				$writeto = fopen($new_filename, "x");

				for ($y = $file_chunks[$i]['start']; $y <= $file_chunks[$i]['end']; $y++) {

					self::writeAllThatJazz($writeto, $file_contents, $y);

					$line_counter++;
				}

				fclose($writeto);
				self::output($starting_time, $line_counter, $new_filename);
			}

		} else {

			$new_filename                 = self::newFilename($file, $increment = null);
			$this->file_names_to_index[1] = $new_filename;
			$line_counter                 = 0;

			self::checkFileExists($new_filename);

			$writeto = fopen($new_filename, "x");

			for ($y = 0; $y <= $lines-1; $y++) {

				self::writeAllThatJazz($writeto, $file_contents, $y);

				$line_counter++;
			}

			fclose($writeto);

			self::output($starting_time, $line_counter, $new_filename);

			return $new_filename;
		}
	}

	
    /**
	 * @var $writeto
	 * @var $file_contents
	 * @var $y
	 **/
	function writeAllThatJazz($writeto, $file_contents, $y) {
		$brake_down = preg_split("/[{}:\s,]+/", $file_contents[$y]);
		$id         = $brake_down[2];
		$index      = '{'.'"index"'.':'.'{'.'"_id"'.':'.$id.'}}';
		$file_line  = trim($file_contents[$y], " ,[]");
		$line       = $index."\n".$file_line."\n";

		fwrite($writeto, $line);
	}

	
    /**
	 * @var $file
	 **/
	function evaluateSize($file) {
		$file_size      = self::getSize($file);
		$fileSize_limit = 75000000;
		$no_of_files    = 1;

		if ($file_size > $fileSize_limit) {
			$no_of_files = 2;
			while (!isset($stop)) {
				$file_chunk = $file_size/$no_of_files;
				if ($file_chunk < $fileSize_limit) {
					$stop = true;
				} else {
					$no_of_files++;
				}
			}
		}

		return $no_of_files;
	}

	
    /**
	 * @var $file
	 **/
	function getSize($file) {
		return round(filesize($file), 0);
	}

	
    /**
	 * @var $file
	 * @var $i
	 **/
	function newFilename($file, $increment = null) {
		return (stristr($file, '.', true)."_new".$increment.".json");
	}

	
    /**
	 * @var $new_filename
	 **/
	function checkFileExists($new_filename) {
		if (is_file($new_filename)) {
			unlink($new_filename);
		}
	}

	
    /**
	 * @var $starting_time
	 **/
	function calculateTime($starting_time) {
		$ending_time = gettimeofday();
		$time        = [];

		$elapsed_time = $ending_time['sec']-$starting_time['sec'];

		$hour = $elapsed_time/3600%24;
		$mins = $elapsed_time/60%60;
		$secs = $elapsed_time%60;

		return $time = ['h' => $hour, 'm' => $mins, 's' => $secs];
	}

	
    /**
	 * @var $file
	 * @var $lines
	 **/
	function calculateChunks($file, $lines) {
		$that_many_files = self::evaluateSize($file);
		$chunk_of_lines  = round($lines/$that_many_files, 0);
		$line_chunks     = [];

		$line_chunks[1] =
		[
			'start' => 0,
			'end'   => $chunk_of_lines
		];

		$start_of_last_chunk = $line_chunks[1]['end']+1;

		if ($that_many_files >= 2) {
			for ($i = 2; $i <= $that_many_files-1; $i++) {

				$previous = $i-1;

				$line_chunks[$i] =
				[
					'start' => $line_chunks[$previous]['end']+1,
					'end'   => $line_chunks[$previous]['end']+$chunk_of_lines+1
				];

				$start_of_last_chunk = $line_chunks[$previous]['end']+$chunk_of_lines;
			}
		}

		$line_chunks[$that_many_files] =
		[
			'start' => $start_of_last_chunk,
			'end'   => $lines-1
		];

		return $line_chunks;
	}

	
    /**
	 * @var $new_filename
	 * @var $lines
	 * @var $starting_time
	 **/
	function output($starting_time, $lines, $new_filename) {
		$overal_time = self::calculateTime($starting_time);

		echo "\n".'=== added: '.$lines.' index lines'.' to '.$new_filename.' in '.$overal_time['h'].':'.$overal_time['m'].':'.$overal_time['s'].' ==='."\n";
	}

}
