<?php

namespace SearchBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteIndexCommand extends Command
{
    // the name of the command (the part after "bin/console")
    
    protected static $defaultName = 'search:delete-index';
         
    public  $fail;
    private $server;
    private $port;  

     function __construct( $elastic_server, $elastic_port )
    {            
        $this->server = $elastic_server;
        $this->port = $elastic_port;

        parent::__construct();
    }    

    protected function configure()
    {
        $this
        ->setDescription('Removes an elastic-search index from the elastic-search server')
        ->setHelp('This command allows you to delete an elastic search index' )      
        ->addArgument( 'index', InputArgument::REQUIRED, 'the elastic-search index to delete' );
    }

    protected function execute( InputInterface $input, OutputInterface $output )
    {
    	$index = trim( $input->getArgument( 'index' ) );

		echo "\n" . '=== index ' . $index . ' will be deleted !!!!, procced ? ' . "\n" . '=== y/n ? : ';
		$this->ask_confirmation( $index );

		$this->deleteIndex( $index );

        if (!$this->fail) {
            $output->writeln( '=== index ' . $index . ' deleted ===' );
            die;           
        } else {
            $output->writeln( '=== Error ' . $this->fail . '===' );
            die; 
        }        
	}


	function ask_confirmation()
	{
    	$answer = trim( strtolower( fgets(STDIN) ) );
		$this->check_confirmation( $answer );
    }


    function check_confirmation( $answer ) {
        switch ($answer) {
            case 'y':
                return;	
                break;
            case 'n':
                echo '=== terminating...'; die;
                break;
            
            default:
                echo '=== please enter y or n ';
                $this->ask_confirmation();
                break;
        }
    }	


	/**
	 * summary: deletes an index
	 * @var $index
	 **/
	protected function deleteIndex( $index )
	{
		$request = curl_init();

		$url     =  $this->server. ':' . $this->port  . '/' . $index . '?pretty';
		$options = [
			CURLOPT_URL            => $url,
			CURLOPT_CUSTOMREQUEST  => 'DELETE',
			CURLOPT_RETURNTRANSFER => 1
		];

		curl_setopt_array($request, $options);

		$result = curl_exec($request);

		if (curl_errno($request)) {
			echo 'Error:' . curl_error($request);
		}

		//print_r(curl_getinfo($request));//testing

        $this->fail = curl_error($request);

		curl_close($request);
	}
}
