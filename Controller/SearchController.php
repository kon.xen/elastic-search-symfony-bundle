<?php

namespace SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use SearchBundle\DependencyInjection\SearchExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * summary
 */
class SearchController extends controller
{   
    private $server;
    private $port;    
  
    function __construct( $elastic_server, $elastic_port )
    {
       //$loader = new SearchExtension;           
        $this->server = $elastic_server;
        $this->port = $elastic_port;
    }


    public function guiAction() 
    {     
        $html = $this->render(
            'SearchBundle:Default:search.html.twig',
            []
        );

        return new Response($html);
    }   


    /**
     * summary: json body query. searches specified indicies, filtered.
     * @var $index
     * @var $search_for
     **/
    public function retrieveAction( ) 
    {           
        $results = [];
        $refined = [];
        $indicies = 'worlds,articles,campaigns,users';
        $no_of_results = 1000;

        if ( strlen($_POST['maxresults']) > 1 ) {
            $no_of_results = $_POST['maxresults'];
        }

        if (isset( $_POST['world_filter'] )) {
           $refined[0] = 'worlds';            
        }
        if (isset( $_POST['article_filter'] )) {
             $refined[1] = 'articles';            
        }
        if (isset( $_POST['campaign_filter'] )) {
             $refined[2] = 'campaigns';            
        } 
        if (isset( $_POST['user_filter'] )) {
             $refined[3] = 'users';            
        } 
        
        if (!empty($refined)) {
            $indicies = implode( ",", $refined );            
        }

        // var_dump($indicies);
        
        $search_for = trim( $_POST['question']);
        
        $raw_results = $this->search_filtered( $search_for, $indicies, $no_of_results );
       
        if (!$raw_results) {
            
            $hits = ['results' => 0];
            $no_of_results = count($hits) - 1;     
             
        } else {
             //var_dump($raw_results);

            $results = json_decode ( $raw_results, JSON_OBJECT_AS_ARRAY );
            $hits = $results['hits']['hits'];
            $no_of_results = count($hits);
        }
        
        $html = $this->render(
            'SearchBundle:Default:results.html.twig',
            [
                'hits'          => $hits,
                'whatofind'     => $search_for,
                'noOfResults'   => $no_of_results, 
            ]
        );

        return new Response($html);
    }



     public function search_filtered( $search_for, $indicies, $no_of_results )
     {
        $request = curl_init();
        // older filter"should": [                  
        //                         {"match" : { "state" : "public"}},                     
        //                         {"match" : { "enabled" : 1 }}
        //             ],
        
        $filter = '"must_not" : [  
                                {"match" : { "state" : "archived"}},                     
                                {"match" : { "state" : "private" }},
                                {"match" : { "enabled" : 0 }}
                    ]';

        $query_multi_match = '"multi_match" : {
                                "query" : "' . $search_for . '",
                                "fields": [ "name", "descripton", "excerpt", "username", "bio" ] 
                            }';

        $search = '{
                        "query" : {
                            "bool" : {
                                "must" : {
                                    ' . $query_multi_match .'
                                },
                                '. $filter. '
                            }
                        }
                    }';
    

       $url = $this->server. ':' . $this->port  . '/' . $indicies . '/_search?size=' . $no_of_results ;
       
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_POST => 1, //works wether on or off
            //CURLOPT_CUSTOMREQUEST => 'GET', //works wether on or off
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => ['Content-Type:application/x-ndjson'],
            CURLOPT_POSTFIELDS => $search,
        ]; 

            
        curl_setopt_array($request, $options);

        $result = curl_exec($request);

        if (curl_errno($request)) {
            echo 'Error:' . curl_error($request);
        }    

        curl_close($request);

        return $result;
     }

}
