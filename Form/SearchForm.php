<?php 

namespace SearchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
#use Symfony\Component\Form\Extension\Core\Type\FileType;
#use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
#use Symfony\Component\Validator\Constraints\IsTrue;

//-------------------------------------------------------------------

class SearchForm extends AbstractType
{
	// overriding two methods from the parent class..

	public function buildForm(FormbuilderInterface $builder, array $options)
	{
		$builder->add('primary', TextType::class, ['label' => 'Primary']);
				//->add('secondary',TextType::class, ['label' => 'Secondary']);
	}


	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(['data_class' => null ]);
	}
}
