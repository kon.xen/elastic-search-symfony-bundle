<?php
namespace SearchBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\PersistentCollection;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Asset\Package;

use ContentBundle\Entity\Article as Article;
use CampaignBundle\Entity\Campaign as Campaign;
use WorldBundle\Entity\World as World;
// use AppBundle\Entity\User as User; // what entity do i use for that?

use AppBundle\Service\WordCounter;
use AppBundle\Service\NotificationGenerator;


class SearchElasticSubscriber implements EventSubscriber
{
    private $entity;
    private $em;
    private $counter;
    private $notification;
    private $token;
    private $loggedInUser;

    private $server;
    private $port;    
  
    function __construct( $elastic_server, $elastic_port )
    {
       //$loader = new SearchExtension;           
       $this->server = $elastic_server;
       $this->port = $elastic_port;
       //var_dump( $this->server, $this->port);
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }


     public function postUpdate( LifecycleEventArgs $args )
    {
        $this->index( $args );
    }


    public function postPersist( LifecycleEventArgs $args )
    {
        $this->index( $args );
    }


     public function index( LifecycleEventArgs $args )
    {
        $entity = $args->getObject();
        $this->entity = $entity;

        $em = $args->getEntityManager(); 
        $this->em = $em;

        $uow = $em->getUnitOfWork(); //do i really need it ?        
              
        if ($entity instanceof Article) { 

            $index = 'articles';
            
            $doc_id = $this->entity->getId();

            $fields = [
                'doc' => [ 
                        'title' => $this->entity->getTitle(),
                        'content' => $this->entity->getContent(),
                        'template' => $this->entity->getTemplateType(),
                        'state' => $this->entity->getState()
                ],                
                'doc_as_upsert' => true
            ];

         $this->updateElasticIndex( $index , $doc_id , $fields );        
        }

        if ($entity instanceof Campaign) {

            $index = 'campaigns';
            
            $doc_id = $this->entity->getId();

            $fields = [
                'doc' => [ 
                        'name' => $this->entity->getName(),
                        'description' => $this->entity->getDescription(),
                        'excerpt' => $this->entity->getExcerpt(),
                        'state' => $this->entity->getState()
                ],                
                'doc_as_upsert' => true
            ];

        $this->updateElasticIndex( $index , $doc_id ,  $fields );
        }

        if ($entity instanceof World) {

            $index = 'worlds';
            
            $doc_id = $this->entity->getId();

            $fields = [
                'doc' => [ 
                        'name' => $this->entity->getName(),
                        'description' => $this->entity->getDescription(),                        
                        'state' => $this->entity->getState()
                ],                
                'doc_as_upsert' => true
            ];           

        $this->updateElasticIndex( $index , $doc_id ,  $fields );

        }  
    }


    function updateElasticIndex( $index = '' , $doc_id ,  $fields )
    {
      
        if ($index == '') {
            return;            
        }


        $url = $this->server . ':' . $this->port . '/' . $index .'/_update/' . $doc_id ;

        $options = [
            CURLOPT_URL            => $url,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => json_encode( $fields ),
            CURLOPT_HTTPHEADER     => ['Content-Type: application/x-ndjson'],
            CURLOPT_RETURNTRANSFER => 1
        ];

        $request = curl_init();

        curl_setopt_array($request, $options);

        $result = curl_exec($request);

        if (curl_errno($request)) {
            echo 'Error:' . curl_error($request);
        }   

        curl_close($request);

        // echo $result; die;
        // echo $this->entity->getName();
        // echo $this->entity->getDescription();//die; //testing
    }


    /**
     * summary: deletes an index
     * @var $index
     **/
    function deleteIndex( $index )
    {
        $request = curl_init();

        $url     = $this->server . ':' . $this->port . '/' . $index . '?pretty';
        $options = [
            CURLOPT_URL            => $url,
            CURLOPT_CUSTOMREQUEST  => 'DELETE',
            CURLOPT_RETURNTRANSFER => 1
        ];

        curl_setopt_array($request, $options);

        $result = curl_exec($request);

        if (curl_errno($request)) {
            echo 'Error:' . curl_error($request);
        }

        //print_r(curl_getinfo($request));//testing

        curl_close($request);
    }
}
