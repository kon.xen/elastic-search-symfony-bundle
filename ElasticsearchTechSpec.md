# Elastic search technical specification.

### Developers

Developer  : *Constantinos* 
Supervisor : *Dimitris*
Consiglieri: *Ian*


### Overview

Develop a module in *Symfony* for World anvil to use *ElasticSearch*  
as a search engine to allow a loged-in user to search World Anvil content.


### Context

Curent search is relativelly slow we need a much faster one that will  
remain effective as world anvil grows. (Also will provide with usefull  
usage data.)


### Goals  

**Make it work and work fast!**  ;)
  
* Create index and documents in elasticsearch. 
	* would have to run only once at deployment. and if ever poop hit the fan and we need to re-index
	* should be achievable with CURL commands... ( see elasticSearch documentation)
	*Create a script for populating the indexes from ***WA DB*** 
	* use  *.csv* or  *.sql* file to extract from ? Dimitri i canot put my finger on what was decided last time we met. 
	* Jason can be used but need to process / format the exported files from SQL so that eleastic can import them
	* Only populate indexes with articles that have status *public*.  
	* Searchable fields:
		* Creator name
		* World name
		* Character name
		* Genre
		* description or prose ? (can't remember )
* Create an event listener for 
	* create article
	* update article
	* delete article
	* change in status , *public / private*	
* Do the above actions need to be registered as events? (need to look it up and understand it better)
* Return results and render with twig
* ***Is there a need for advanced search options ?***
* "Integrate" module with rest of WA  


### Milestones
---	

* ~~Understand CURL (*PHP module, allready used in the WA*)
* ~~Create a script for creating indexes. (CURL)~~
* ~~Create a script for and populating the indexes with data from WA DB 
* ~~Create queries for searching elastic (CURL)
* Aggregation! don't think are needed...
* ~~Make searches (CURL) 50%
* Make event listener to listen for updates and send updates to elastic indexes. (PHP / CURL)
* Make GUI ?


## Current solution with *FOS Elastica*

Had a working basic search with *ES 6.7* and *FOSelastica*. 


#### Problems.

We have a botle neck on creating indecies at dev enva and slow result and incopatibility  
between versions of FOSelastica & elastic search  & Symfony dependecies.  

**In more detail...**

We are using *Symfony 3.4.26* ( latest 4.3 )

*Elastic search* is now at Version 7.*

We are using FOS Elastica Bundle for all the easticsearch jazz.

* *FE* is supporting *elastic search* up to ver 6., *symfony* up to 4 and *PHP 7.1*
* *FE* is already kinda full of deprecations... (and has 100+ plus issues raised on github).  
	Also is using Types when types are deprecated from elastic search 6 onward., making it awkward to set up.
	* elastic search does not accept more than one types / index...
	* Had to turn all types to indexes.
* articles are generating error when populating the indexes.
	`` Maximum function nesting level of '256' reached, aborting!"   
		["exception" => Error { …},"command" => "fos:elastica:populate",  
		"message" => "Maximum function nesting level of '256' reached, aborting!"] []  
		In NormalizerFormatter.php line 192:
		Maximum function nesting level of '256' reached, aborting! " ``
	
So looked in to making indexing faster in Dev. env ...

* WA stack has PHP 7.x. that limits to Enqueue ver 8.
* All documentation is for enqueue 9 that needs PHP 7.3 (I think..)
* Documentation of elastica on enqueue is not good. and vailable only for ver 8
* Nearly impossible for the junior developer to set it up
* Also not sure what transport to use on the set up nor how to configure the transport. Assumed is Dbal.

Even if I did make it run we would still have to use a different solution for the production env. 
Threfore dropped it and used only The indexes I could populate. 

***If we were using Flex there wouldn't be a problem with all the above ...***


### Proposed solution  

* Use elasticsearch running on a server somewhere...
* Make use of the REST API for creating indexes, searching and updating the database.
	* will need *CURL* and *elasticsearch* bundles installed in *Symfony*.


### Discussion

> Everything is open for discussion !
> I wrote everything down from memmory so please come back with comments.


### Scope and timeline
- Have to go through all elasticSearch documentation again - 2 days
- Change brain from design back to coding 	 - +3 days
- Write something that works                 - +7 days
	- indexes
	- populate 
	- query
	- listener
- Break it and try to understand what the hell I did to break it and fix it - +3 days
- Have something to show - +5 days


> will start ASAP (Possibly 12th of August) then  
- 3.5 weeks after that to complete in Ideal conditions
- 6.0 weeks in normal conditions
- 10 weeks in catastrophic conditions 


#### May the force be with us all.
