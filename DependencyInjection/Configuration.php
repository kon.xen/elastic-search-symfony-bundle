<?php

namespace SearchBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;


class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
   {     
        $treeBuilder = new TreeBuilder();
	        $rootNode = $treeBuilder->root('search');

			$rootNode
            ->children()
                ->arrayNode('elastic_search')
                    ->children()
                        ->scalarNode('elastic_host')
                        	->info('This is the host url for the elastic search server.')
			            	->isrequired()
			            ->end()
                        ->scalarNode('elastic_port')
                        	->info('This is the port number for the elastic search server.')
			            	->isrequired()
			            ->end()
                    ->end()
                ->end() // elastic
                ->arrayNode('command')
                    ->children()
                        ->scalarNode('json_path')
                            ->info('This is the default path for keeping the json files used to populate elastic search indicies')
                            ->isrequired()
                        ->end()                        
                    ->end()
                ->end() // command
            ->end();
            
        return $treeBuilder;
    }
}
