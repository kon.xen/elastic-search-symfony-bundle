<?php

namespace SearchBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;


class SearchExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {        
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);    
    }
    
}
